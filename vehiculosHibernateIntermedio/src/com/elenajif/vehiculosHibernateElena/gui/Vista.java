package com.elenajif.vehiculosHibernateElena.gui;

import com.github.lgooddatepicker.components.DateTimePicker;

import javax.swing.*;

/**
 * Created by Profesor on 17/02/2020.
 */
public class Vista {
    private JFrame frame;
    private JPanel panel1;

    JTextField txtId;
    JTextField txtMatricula;
    JTextField txtMarca;
    JTextField txtModelo;
    JTextField txtPropietario;

    DateTimePicker dateTimePicker;

    JButton altaButton;
    JButton listarCochesButton;
    JButton modificarButton;
    JButton borrarButton;
    JButton listarPropietariosButton;

    JList listCoches;
    JList listPropietarios;
    JList listCochesPropietarios;

    DefaultListModel dlm;
    DefaultListModel dlmPropietarios;
    DefaultListModel dlmCochesPropietario;

    JMenuItem conexionItem;
    JMenuItem salirItem;

    public Vista() {
        frame = new JFrame("Vista");
        frame.setContentPane(panel1);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);

        crearMenu();
        crearModelos();

        frame.setLocationRelativeTo(null);
    }

    private void crearModelos() {
        dlm = new DefaultListModel();
        listCoches.setModel(dlm);
        dlmPropietarios = new DefaultListModel();
        listPropietarios.setModel(dlmPropietarios);
        dlmCochesPropietario = new DefaultListModel();
        listCochesPropietarios.setModel(dlmCochesPropietario);
    }
    private void crearMenu() {
        JMenuBar barra = new JMenuBar();
        JMenu menu = new JMenu("Archivo");

        conexionItem=new JMenuItem("Conectar");
        conexionItem.setActionCommand("Conectar");

        salirItem  = new JMenuItem("Salir");
        salirItem  .setActionCommand("Salir");

        menu.add(conexionItem);
        menu.add(salirItem);
        barra.add(menu);
        frame.setJMenuBar(barra);
    }

}
