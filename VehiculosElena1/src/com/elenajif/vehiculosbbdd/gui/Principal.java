package com.elenajif.vehiculosbbdd.gui;

/**
 * Created by Profesor on 02/12/2019.
 */
public class Principal {
    public static void main(String args[]) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
