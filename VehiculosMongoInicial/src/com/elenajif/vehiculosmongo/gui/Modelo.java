package com.elenajif.vehiculosmongo.gui;

import com.elenajif.vehiculosmongo.base.Coche;
import com.elenajif.vehiculosmongo.util.Util;
import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Modelo {


    public void conectar() {
    }

    public void desconectar(){
    }


    public void guardarCoche(Coche unCoche) {
    }

    public List<Coche> getCoches(){
        ArrayList<Coche> lista = new ArrayList<>();
        return lista;
    }

    /**
     * Listo los coches atendiendo a un 2 criterios basados en expresiones regulares.
     * @param text cadena que quiero que aparezca en las matriculas o marcas
     * @return
     */
    public List<Coche> getCoches(String text) {
        ArrayList<Coche> lista = new ArrayList<>();
        return lista;
    }

    public Document cocheToDocument(Coche unCoche){
        Document documento = new Document();
        return documento;
    }

    public Coche documentToCoche(Document document){
        Coche unCoche = new Coche();
        return unCoche;
    }

    public void modificarCoche(Coche unCoche) {
    }

    public void borrarCoche(Coche unCoche) {
    }


}
