package main;

/**
 * Created by Profesor on 10/01/2020.
 */
import gui.Controlador;
import gui.Modelo;
import gui.Vista;

public class Principal {
    public static void main(String[] args) {
        Modelo modelo = new Modelo();
        Vista vista= new Vista();
        Controlador controlador = new Controlador(modelo,vista);
    }
}
