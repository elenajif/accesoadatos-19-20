package gui;

import java.io.*;
import java.sql.*;
import java.util.Properties;

/**
 * Created by Profesor on 10/01/2020.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }
    String getUser() {
        return user;
    }
    String getPassword() {
        return password;
    }
    String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;

    void conectar() throws IOException {
        try {
            conexion= DriverManager.getConnection("jdbc:mysql://"+ip+":3306/mibase",user,password);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        PreparedStatement statement=null;
        String code=leerFichero();
        String[] query = code.split("--");
        for (String aQuery:query) {
            try {
                statement = conexion.prepareStatement(aQuery);
                statement.executeQuery();
            } catch (SQLException e) {
                e.printStackTrace();
            }
            assert statement!=null;
            try {
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    private String leerFichero() throws IOException{
            BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"));
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea=reader.readLine())!=null) {
                 stringBuilder.append(linea);
                 stringBuilder.append(" ");
                }
        return stringBuilder.toString();
    }

    void desconectar() {
        try {
            conexion.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        conexion=null;
    }

    void insertarEditorial (String editorial, String email, String telefono, String tipoEditorial, String web) throws SQLException {
        String sentenciaSql="INSERT INTO editoriales (editorial, email, telefono, tipoEditorial, web) VALUES (?,?,?,?,?)";
        PreparedStatement sentencia=null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1,editorial);
        sentencia.setString(2,email);
        sentencia.setString(3,telefono);
        sentencia.setString(4,tipoEditorial);
        sentencia.setString(5, web);
        sentencia.executeQuery();
            if (sentencia != null) {
                sentencia.close();
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

    }

    void insertarAutor() {

    }

    void insertarLibro() {

    }

    void borrarEditorial (int idEditorial) throws SQLException {
        String sentenciaSql="DELETE FROM editoriales WHERE ideditorial=?";
        PreparedStatement sentencia=null;
        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        sentencia.setInt(1,idEditorial);
        sentencia.executeQuery();

    }

    void borrarAutor () {

    }

    void borrarLibro() {

    }

    void modificarEditorial(String editorial, String email, String telefono, String tipoEditorial, String web,int ideditorial) throws SQLException {
        String sentenciaSql="UPDATE editoriales SET editorial=?, email=?,telefono=?,tipoEditorial=?,web=? WHERE ideditorial=?";
        PreparedStatement sentencia=null;
        sentencia=conexion.prepareStatement(sentenciaSql);
        sentencia.setString(1,editorial);
        sentencia.setString(2,email);
        sentencia.setString(3,telefono);
        sentencia.setString(4,tipoEditorial);
        sentencia.setString(5, web);
        sentencia.setInt(6,ideditorial);
        sentencia.executeUpdate();
    }

    void modificarAutor() {

    }

    void modificarLibro() {

    }

    ResultSet consultarEditorial() throws SQLException {
        String sentenciaSql="SELECT concat(ideditorial) as 'ID',concat(editorial) as 'Nombre editorial',concat(email) as 'email',concat(telefono) as 'Teléfono',concat(tipoEditorial) as 'Tipo', concat(web) as 'web' FROM editoriales";
        PreparedStatement sentencia=null;
        ResultSet resultado= null;
        try {
            sentencia=conexion.prepareStatement(sentenciaSql);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        resultado=sentencia.executeQuery();
        return   resultado;
    }

    void consultarLibro() {

    }

    void consultarAutor() {

    }

    public boolean editorialNombreYaExiste(String nombre) throws SQLException {
        String editorialNameConsult ="SELECT existeNombreEditorial(?)";
        PreparedStatement function;
        boolean nameExists=false;
        function=conexion.prepareStatement(editorialNameConsult);
        function.setString(1,nombre);
        ResultSet rs =function.executeQuery();
        rs.next();
        nameExists=rs.getBoolean(1);

        return nameExists;
    }

    void libroIsbnYaExiste() {

    }

    void autorNombreYaExiste() {

    }

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip   ip de la bbdd
     * @param user user de la bbdd
     * @param pass contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }


}
