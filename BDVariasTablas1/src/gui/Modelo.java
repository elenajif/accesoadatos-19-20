package gui;

import java.io.*;
import java.sql.*;
import java.time.LocalDate;
import java.util.Properties;

/**
 * Created by Profesor on 10/01/2020.
 */
public class Modelo {
    private String ip;
    private String user;
    private String password;
    private String adminPassword;

    /**
     * Constructor de la clase, inicializa los ArrayLists,
     * carga los datos del fichero properties y setea isChanged a false.
     */
    public Modelo() {
        getPropValues();
    }

    String getIP() {
        return ip;
    }

    String getUser() {
        return user;
    }

    String getPassword() {
        return password;
    }

    String getAdminPassword() {
        return adminPassword;
    }

    private Connection conexion;

    /**
     * Lee el archivo de propiedades y setea los atributos pertinentes
     */
    private void getPropValues() {
        InputStream inputStream = null;
        try {
            Properties prop = new Properties();
            String propFileName = "config.properties";

            inputStream = new FileInputStream(propFileName);

            prop.load(inputStream);
            ip = prop.getProperty("ip");
            user = prop.getProperty("user");
            password = prop.getProperty("pass");
            adminPassword = prop.getProperty("admin");

        } catch (Exception e) {
            System.out.println("Exception: " + e);
        } finally {
            try {
                if (inputStream != null) inputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Actualiza las propiedades pasadas por parámetro del archivo de propiedades
     *
     * @param ip        ip de la bbdd
     * @param user      user de la bbdd
     * @param pass      contraseña de la bbdd
     * @param adminPass contraseña del administrador
     */
    void setPropValues(String ip, String user, String pass, String adminPass) {
        try {
            Properties prop = new Properties();
            prop.setProperty("ip", ip);
            prop.setProperty("user", user);
            prop.setProperty("pass", pass);
            prop.setProperty("admin", adminPass);
            OutputStream out = new FileOutputStream("config.properties");
            prop.store(out, null);

        } catch (IOException ex) {
            ex.printStackTrace();
        }
        this.ip = ip;
        this.user = user;
        this.password = pass;
        this.adminPassword = adminPass;
    }


    void conectar() {

        try {
            conexion = DriverManager.getConnection(
                    "jdbc:mysql://"+ip+":3306/inkverso",user, password);
        } catch (SQLException sqle) {
            try {
                conexion = DriverManager.getConnection(
                        "jdbc:mysql://"+ip+":3306/",user, password);

                PreparedStatement statement = null;

                String code = leerFichero();
                String[] query = code.split("--");
                for (String aQuery : query) {
                    statement = conexion.prepareStatement(aQuery);
                    statement.executeUpdate();
                }
                assert statement != null;
                statement.close();

            } catch (SQLException | IOException e) {
                e.printStackTrace();
            }
        }
    }
    private String leerFichero() throws IOException {
        try (BufferedReader reader = new BufferedReader(new FileReader("basedatos_java.sql"))) {
            String linea;
            StringBuilder stringBuilder = new StringBuilder();
            while ((linea = reader.readLine()) != null) {
                stringBuilder.append(linea);
                stringBuilder.append(" ");
            }

            return stringBuilder.toString();
        }
    }

    void desconectar() {
        try {
            conexion.close();
            conexion = null;
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        }
    }
    void insertarEditorial(String editorial, String email, String telefono, String tipoEditorial, String web) {
        String sentenciaSql = "INSERT INTO editoriales (editorial, email, telefono, tipoeditorial, web) VALUES (?, ?, ?, ?, ?)";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEditorial);
            sentencia.setString(5, web);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    void modificarEditorial(String editorial, String email, String telefono, String tipoEditorial, String web, int ideditorial){

        String sentenciaSql = "UPDATE editoriales SET editorial = ?, email = ?, telefono = ?, tipoeditorial = ?, web = ?" +
                "WHERE ideditorial = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setString(1, editorial);
            sentencia.setString(2, email);
            sentencia.setString(3, telefono);
            sentencia.setString(4, tipoEditorial);
            sentencia.setString(5, web);
            sentencia.setInt(6, ideditorial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }
    void borrarEditorial(int ideditorial) {
        String sentenciaSql = "DELETE FROM editoriales WHERE ideditorial = ?";
        PreparedStatement sentencia = null;

        try {
            sentencia = conexion.prepareStatement(sentenciaSql);
            sentencia.setInt(1, ideditorial);
            sentencia.executeUpdate();
        } catch (SQLException sqle) {
            sqle.printStackTrace();
        } finally {
            if (sentencia != null)
                try {
                    sentencia.close();
                } catch (SQLException sqle) {
                    sqle.printStackTrace();
                }
        }
    }

    ResultSet consultarEditorial() throws SQLException {
        String sentenciaSql = "SELECT concat(ideditorial) as 'ID', concat(editorial) as 'Nombre editorial', concat(email) as 'Email', " +
                "concat(telefono) as 'Teléfono', concat(tipoeditorial) as 'Tipo', concat(web) as 'Web' FROM editoriales";
        PreparedStatement sentencia = null;
        ResultSet resultado = null;
        sentencia = conexion.prepareStatement(sentenciaSql);
        resultado = sentencia.executeQuery();
        return resultado;
    }

    public boolean editorialNombreYaExiste(String nombre) {
        String editorialNameConsult = "SELECT existeNombreEditorial(?)";
        PreparedStatement function;
        boolean nameExists = false;
        try {
            function = conexion.prepareStatement(editorialNameConsult);
            function.setString(1, nombre);
            ResultSet rs = function.executeQuery();
            rs.next();

            nameExists = rs.getBoolean(1);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return nameExists;
    }


}
