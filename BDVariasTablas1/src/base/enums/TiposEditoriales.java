package base.enums;

/**
 * Created by Profesor on 10/01/2020.
 */
public enum TiposEditoriales {
    AUTOREDITOR("Autor-Editor"),
    EDITORIAL("Editorial"),
    ORGANISMOOFICIAL("Organismo Oficial"),
    UNIVERSIDAD("Universidad");

    private String valor;

    TiposEditoriales(String valor) {

        this.valor = valor;
    }

    public String getValor() {

        return valor;
    }

}