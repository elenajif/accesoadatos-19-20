package com.elenajif.vehiculosmongo;

import com.elenajif.vehiculosmongo.gui.Vista;
import com.elenajif.vehiculosmongo.gui.Controlador;
import com.elenajif.vehiculosmongo.gui.Modelo;


public class Principal {
    public static void main(String[] args) {
        Vista vista = new Vista();
        Modelo modelo = new Modelo();
        Controlador controlador = new Controlador(vista, modelo);
    }
}
